<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Workshop Pembuatan Website untuk Bisnis Digital">
	<meta name="author" content="Hamka Mannan">

	<title>Admin Panel</title>

	<!-- Custom fonts for this template-->
	<link href="<?= base_url('themes/sbadmin'); ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="<?= base_url('themes/sbadmin'); ?>/css/sb-admin-2.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">

	<?= $this->renderSection('style'); ?>

</head>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<!-- Sidebar -->
		<?= $this->include('layout/sbadmin/partial/sidebar'); ?>
		<!-- End of Sidebar -->

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<!-- Topbar -->
				<?= $this->include('layout/sbadmin/partial/navigation'); ?>
				<!-- End of Topbar -->

				<!-- Begin Page Content -->
				<div class="container-fluid">
					<?= $this->renderSection('content'); ?>
				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- End of Main Content -->

			<!-- Footer -->
			<?= $this->include('layout/sbadmin/partial/footer'); ?>
			<!-- End of Footer -->

		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>

	<!-- Logout Modal-->
	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
					<a class="btn btn-primary" href="login.html">Logout</a>
				</div>
			</div>
		</div>
	</div>

	<script>
		var ROOTPATH = '/';
		var WRITEPATH = '/writable/';
		var BASEURL = '<?=base_url()?>';
	</script>

	<!-- Bootstrap core JavaScript-->
	<script src="<?= base_url('themes/sbadmin'); ?>/vendor/jquery/jquery.min.js"></script>
	<script src="<?= base_url('themes/sbadmin'); ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="<?= base_url('themes/sbadmin'); ?>/vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom scripts for all pages-->
	<script src="<?= base_url('themes/sbadmin'); ?>/js/sb-admin-2.min.js"></script>


	<!-- Dropzone JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/min/dropzone.min.js"></script>
	<script>
		/* Dropzone*/
		Dropzone.autoDiscover = false;

		function setDropzone(domID, routePath, acceptedFiles = 'application/pdf', maxFiles = 1, maxFilesize = 10, writePath = false) {
			var baseUrl = BASEURL + "/" + routePath;
			if (maxFiles > 1) {
				url = baseUrl + "/do_upload";
				paramName = "file";
			} else {
				url = baseUrl + "/do_upload";
				paramName = "file";
			}

			var myDropzone = new Dropzone('#' + domID, {
				dictDefaultMessage: "<span class='text-muted'><i class='fa fa-cloud'></i><br>DRAG &amp; DROP FILES HERE OR CLICK TO UPLOAD</span>",
				url: url,
				paramName: paramName,
				maxFiles: maxFiles,
				maxFilesize: maxFilesize,
				addRemoveLinks: true,
				acceptedFiles: acceptedFiles,
				renameFile: function(file) {
					var filename = new Date().getTime() + '_' + file.name.toLowerCase().replace(' ', '_');
					console.log("renameFile");
					console.log(filename);

					return filename;
				},
				accept: function(file, done) {
					console.log("accept");
					console.log(file);
					done();
				},
				init: function() {
					this.on("maxfilesexceeded", function(file) {
						console.log("max files exceeded");
						console.log(file);
					});
				},
				success: function(file, response) {
					console.log('success');
					console.log(response);

					var uuid = file.upload.uuid;
					var value = response.data.name;
					var name = domID + '[' + uuid + ']';

					$('#' + domID + '_listed').append('<input type="hidden" name="' + name + '" value="' + value + '" />');
				},
				removedfile: function(file) {
					console.log('removedfile');
					// console.log(file);
					var name = "";

					var path = WRITEPATH + '/' + 'uploads';

					if (writePath) {
						path = writePath;
					}

					if (file.upload !== undefined) {
						name = file.upload.filename;
					} else {
						name = file.name;
						path = ROOTPATH + '/public/uploads/' + routePath;
					}

					$.ajax({
						type: 'POST',
						url: baseUrl + "/do_delete",
						data: "name=" + name + "&path=" + path,
						dataType: 'html'
					});

					$('input[name="' + domID + '[' + file.upload.uuid + ']"]').remove();

					var _ref;
					return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
					return true;

				}
			});

			return myDropzone;
		}
	</script>

	<!-- Toastr JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	<script>
		var toastr_msg = '<?= get_message('toastr_msg'); ?>';
		var toastr_type = '<?= get_message('toastr_type'); ?>';

		if (toastr_msg.length > 0) {
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"newestOnTop": true,
				"progressBar": true,
				"positionClass": "toast-top-full-width",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "3500",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			};
			toastr[toastr_type](toastr_msg, "Information");
		}
	</script>

	<!-- Magnific Popup JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
	<!-- Lazy Load JS -->
	<script src="https://cdn.jsdelivr.net/npm/jquery-lazy@1.7.11/jquery.lazy.min.js"></script>


	<?= $this->renderSection('script'); ?>

</body>

</html>