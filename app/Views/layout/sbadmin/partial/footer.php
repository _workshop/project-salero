<footer class="sticky-footer bg-white">
	<div class="container my-auto">
		<div class="copyright text-center my-auto">
			<span>Copyright &copy; 2023. Salero Restaurant</span>
		</div>
	</div>
</footer>