<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<i class="fas fa-laugh-wink"></i>
		</div>
		<div class="sidebar-brand-text mx-3">Admin Panel</div>
	</a>

	<!-- Divider -->
	<hr class="sidebar-divider my-0">

	<!-- Heading -->
	<div class="sidebar-heading">
		Menu
	</div>
	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item">
		<a class="nav-link" href="<?=base_url('banners')?>">
			<i class="fas fa-fw fa-images"></i>
			<span>Banner</span></a>
	</li>
	
	<li class="nav-item">
		<a class="nav-link" href="<?=base_url('productcategories')?>">
			<i class="fas fa-fw fa-list"></i>
			<span>Kategori Produk</span></a>
	</li>

	<li class="nav-item">
		<a class="nav-link" href="<?=base_url('products')?>">
			<i class="fas fa-fw fa-layer-group"></i>
			<span>Produk</span></a>
	</li>
	
	<li class="nav-item">
		<a class="nav-link" href="<?=base_url('orders')?>">
			<i class="fas fa-fw fa-shopping-cart"></i>
			<span>Pemesanan</span></a>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">

	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>

</ul>