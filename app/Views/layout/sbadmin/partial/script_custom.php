<script>
	/* Dropzone*/
	Dropzone.autoDiscover = false;

	function setDropzone(domID, routePath, acceptedFiles = 'application/pdf', maxFiles = 1, maxFilesize = 10, writePath = false) {
		var baseUrl = BASEURL + "/" + routePath;
		if (maxFiles > 1) {
			url = baseUrl + "/do_upload";
			paramName = "file";
		} else {
			url = baseUrl + "/do_upload";
			paramName = "file";
		}

		var myDropzone = new Dropzone('#' + domID, {
			dictDefaultMessage: "<span class='text-muted'><i class='material-icons mt-2'>cloud_upload</i><br>DRAG &amp; DROP FILES HERE OR CLICK TO UPLOAD</span>",
			url: url,
			paramName: paramName,
			maxFiles: maxFiles,
			maxFilesize: maxFilesize,
			addRemoveLinks: true,
			acceptedFiles: acceptedFiles,
			renameFile: function(file) {
				var filename = new Date().getTime() + '_' + file.name.toLowerCase().replace(' ', '_');
				console.log("renameFile");
				console.log(filename);

				return filename;
			},
			accept: function(file, done) {
				console.log("accept");
				console.log(file);
				done();
			},
			init: function() {
				this.on("maxfilesexceeded", function(file) {
					console.log("max files exceeded");
					console.log(file);
				});
			},
			success: function(file, response) {
				console.log('success');
				console.log(response);

				var uuid = file.upload.uuid;
				var value = response.data.name;
				var name = domID + '[' + uuid + ']';

				$('#' + domID + '_listed').append('<input type="hidden" name="' + name + '" value="' + value + '" />');
			},
			removedfile: function(file) {
				console.log('removedfile');
				// console.log(file);
				var name = "";

				var path = WRITEPATH + '/' + 'uploads';

				if (writePath) {
					path = writePath;
				}

				if (file.upload !== undefined) {
					name = file.upload.filename;
				} else {
					name = file.name;
					path = ROOTPATH + '/public/uploads/' + routePath;
				}

				$.ajax({
					type: 'POST',
					url: baseUrl + "/do_delete",
					data: "name=" + name + "&path=" + path,
					dataType: 'html'
				});

				$('input[name="' + domID + '[' + file.upload.uuid + ']"]').remove();

				var _ref;
				return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
				return true;

			}
		});

		return myDropzone;
	}
</script>

<script>
	var toastr_msg = '<?= get_message('toastr_msg'); ?>';
	var toastr_type = '<?= get_message('toastr_type'); ?>';

	if (toastr_msg.length > 0) {
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": true,
			"progressBar": true,
			"positionClass": "toast-top-full-width",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "3500",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		};
		toastr[toastr_type](toastr_msg, "Information");
	}
</script>