<?php

if (!function_exists('get_ref_table')) {
	function get_ref_table($table, $fields = 'id', $where = null)
	{
		$db = db_connect();
		$baseModel = $db->table($table);
		$query = $baseModel->select($fields);
		$query->distinct();

		if (!empty($where)) {
			$query->where($where);
		}
		return $query->get()->getResult();
	}
}

if (!function_exists('get_ref_single')) {
	function get_ref_single($table, $where = null, $sort = 'ID', $direction = 'DESC')
	{
		$db = db_connect();
		$baseModel = $db->table($table);
		$query = $baseModel->where($where)->orderBy($sort, $direction);
		return $query->get()->getRow();
	}
}

if (!function_exists('get_pad_number')) {
	function get_pad_number($counter, $prefix = 'TRX-', $zero_length = 4)
	{
		$doc_number = strtoupper($prefix) . str_pad($counter, $zero_length, "0", STR_PAD_LEFT);
		return $doc_number;
	}
}

if (!function_exists('get_dropdown')) {
	function get_dropdown($table, $where = null, $code = 'id', $text = 'name')
	{
		$db = db_connect();
		$baseModel = $db->table($table);
		$query = $baseModel;
		$query->select("$code as code");
		$query->select("$text as text");
		$query->distinct();

		if (!empty($where)) {
			$query->where($where);
		}
		return $query->orderBy($code)->get()->getResult();
	}
}


if (!function_exists('formatRupiah')) {
    function formatRupiah($angka = null, $prefix = 'Rp. ')
    {
        $hasil = $prefix. number_format($angka, 0, ',', '.');
        return $hasil;
    }
}

if (!function_exists('unformatRupiah')) {
    function unformatRupiah($amount = null)
    {
		$result = str_replace("Rp ", "", $amount);
		$result = str_replace(".", "", $result);

        return $result;
    }
}

if (!function_exists('set_message')) {
    function set_message($name, $value)
    {
        $session = \Config\Services::session();
        $session->setFlashdata($name, $value);
    }
}

if (!function_exists('get_message')) {
    function get_message($name)
    {
        $session = \Config\Services::session();
        return $session->getFlashdata($name);
    }
}