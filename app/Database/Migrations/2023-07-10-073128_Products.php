<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Products extends Migration
{
    public function up()
	{
		$this->forge->addField([
			'id'=> ['type' => 'INT','constraint' => 5,'unsigned'=> true,'auto_increment' => true,],
			'name'  => ['type'=> 'VARCHAR','constraint' => '100',],
			'price' => ['type'=> 'DECIMAL','constraint' => '10,2',],
			'product_category_id' => ['type' => 'INT','constraint' => 5,'unsigned'=> true,'default'=> 0,],
			'thumbnail' => ['type'=> 'VARCHAR','constraint' => '255','null'=> true,],
			'image_url' => ['type'=> 'VARCHAR','constraint' => '255','null'=> true,],
			'description' => ['type' => 'TEXT',],
			'active' => ['type'=> 'INT',],
			'created_at'=> ['type' => 'DATETIME',],
			'updated_at'=> ['type' => 'DATETIME',],
			'deleted_at'=> ['type' => 'DATETIME',],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('products');
	}

	public function down()
	{
		$this->forge->dropTable('products');
	}
}
