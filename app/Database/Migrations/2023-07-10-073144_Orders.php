<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Orders extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'=> ['type' => 'INT','constraint' => 5,'unsigned'=> true,'auto_increment' => true,],
            'customer_phone' => ['type' => 'VARCHAR','constraint' => '20',],
            'date' => ['type' => 'DATE',],
            'amount'  => ['type' => 'DECIMAL','constraint' => '10,2', ],
			'status'  => ['type'=> 'VARCHAR', 'constraint' => '255'],
			'created_at'=> ['type' => 'DATETIME',],
			'updated_at'=> ['type' => 'DATETIME',],
			'deleted_at'=> ['type' => 'DATETIME',],
        ]);
        
        $this->forge->addKey('id', true);
        $this->forge->createTable('orders');
    }

    public function down()
    {
        $this->forge->dropTable('orders');
    }
}
