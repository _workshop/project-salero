<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class OrderDetails extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id'=> ['type' => 'INT','constraint' => 5,'unsigned'=> true,'auto_increment' => true,],
            'order_id' => ['type' => 'INT',],
            'product_id' => ['type' => 'INT',],
            'price'  => ['type' => 'DECIMAL','constraint' => '10,2', ],
            'qty'  => ['type' => 'INT', ],
			'created_at'=> ['type' => 'DATETIME',],
			'updated_at'=> ['type' => 'DATETIME',],
			'deleted_at'=> ['type' => 'DATETIME',],
        ]);
        
        $this->forge->addKey('id', true);
        $this->forge->createTable('order_details');
    }

    public function down()
    {
        $this->forge->dropTable('order_details');
    }
}
