<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ProductCategories extends Migration
{
    public function up()
    {
        $this->forge->addField([
			'id'=> ['type' => 'INT','constraint' => 5,'unsigned'=> true,'auto_increment' => true,],
			'name'  => ['type'=> 'VARCHAR','constraint' => '100',],
			'thumbnail' => ['type'=> 'VARCHAR','constraint' => '255','null'=> true,],
			'image_url' => ['type'=> 'VARCHAR','constraint' => '255','null'=> true,],
			'description' => ['type' => 'TEXT',],
			'active' => ['type'=> 'INT',],
			'created_at'=> ['type' => 'DATETIME',],
			'updated_at'=> ['type' => 'DATETIME',],
			'deleted_at'=> ['type' => 'DATETIME',],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('product_categories');
    }

    public function down()
    {
        $this->forge->dropTable('product_categories');
    }
}
