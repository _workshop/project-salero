<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class OrderDetailsSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'order_id' => 1,
                'product_id' => 1,
                'price' => '25000.00',
                'qty' => 2,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ],
            [
                'order_id' => 1,
                'product_id' => 2,
                'price' => '20000.00',
                'qty' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ],
        ];

        $this->db->table('order_details')->insertBatch($data);
    }
}
