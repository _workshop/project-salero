<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class BannersSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'name' => 'Promo Sale',
                'thumbnail' => 'promo_sale_thumbnail.jpg',
                'image_url' => 'promo_sale.jpg',
                'description' => 'Get up to 50% off on selected items!',
                'active' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ],
            [
                'name' => 'New Arrivals',
                'thumbnail' => 'new_arrivals_thumbnail.jpg',
                'image_url' => 'new_arrivals.jpg',
                'description' => 'Check out our latest collection of products!',
                'active' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null,
            ],
        ];

        $this->db->table('banners')->insertBatch($data);
    }
}
