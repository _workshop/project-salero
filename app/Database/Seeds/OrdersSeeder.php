<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class OrdersSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'customer_phone' => '081234567890',
                'date'     => '2023-07-09',
                'amount'   => 150000,
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ],
            [
                'customer_phone' => '089876543210',
                'date'     => '2023-07-10',
                'amount'   => 250000,
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ],
            [
                'customer_phone' => '081111111111',
                'date'     => '2023-07-11',
                'amount'   => 500000,
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ],
            [
                'customer_phone' => '082222222222',
                'date'     => '2023-07-12',
                'amount'   => 300000,
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ],
            [
                'customer_phone' => '083333333333',
                'date'     => '2023-07-13',
                'amount'   => 200000,
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ],
            [
                'customer_phone' => '084444444444',
                'date'     => '2023-07-14',
                'amount'   => 450000,
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ],
            [
                'customer_phone' => '085555555555',
                'date'     => '2023-07-15',
                'amount'   => 700000,
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ],
            [
                'customer_phone' => '086666666666',
                'date'     => '2023-07-16',
                'amount'   => 800000,
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ],
            [
                'customer_phone' => '087777777777',
                'date'     => '2023-07-17',
                'amount'   => 350000,
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ],
            [
                'customer_phone' => '088888888888',
                'date'     => '2023-07-18',
                'amount'   => 900000,
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ],
            [
                'customer_phone' => '089999999999',
                'date'     => '2023-07-19',
                'amount'   => 600000,
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ],
            [
                'customer_phone' => '081212121212',
                'date'     => '2023-07-20',
                'amount'   => 400000,
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ],
        ];

        $this->db->table('orders')->insertBatch($data);
    }
}
