<?php

namespace Products\Models;

use CodeIgniter\Model;

class ProductModel extends Model
{
    protected $table      = 'products';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name', 'price', 'description', 'thumbnail,', 'created_at', 'updated_at'];
	protected $returnType = 'object';
    protected $protectFields = false;
}
