<?php

namespace Products\Controllers\Api;

use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use Products\Models\ProductModel;
use \Hermawan\DataTables\DataTable;

class ProductsController extends BaseController
{
    use ResponseTrait;

	function __construct()
	{
		include APPPATH . 'Config/cors.php';
	}
	
    public function index()
    {
        $model = new ProductModel();
        $products = $model->findAll();

        return $this->respond($products);
    }

    public function show($id)
    {
        $model = new ProductModel();
        $product = $model->find($id);

        if ($product) {
            return $this->respond($product);
        } else {
            return $this->failNotFound('Product not found');
        }
    }

    public function create()
    {
        $model = new ProductModel();
        $data = [
            'name' => $this->request->getVar('name'),
            'price' => $this->request->getVar('price'),
            'description' => $this->request->getVar('description')
        ];

        $model->insert($data);

        $response = [
            'status' => 201,
            'error' => null,
            'message' => 'Product created successfully'
        ];

        return $this->respondCreated($response);
    }

    public function update($id)
    {
        $model = new ProductModel();
        $data = [
            'name' => $this->request->getVar('name'),
            'price' => $this->request->getVar('price'),
            'description' => $this->request->getVar('description')
        ];

        $model->update($id, $data);

        $response = [
            'status' => 200,
            'error' => null,
            'message' => 'Product updated successfully'
        ];

        return $this->respond($response);
    }

    public function delete($id)
    {
        $model = new ProductModel();
        $model->delete($id);

        $response = [
            'status' => 200,
            'error' => null,
            'message' => 'Product deleted successfully'
        ];

        return $this->respondDeleted($response);
    }

	public function datatable()
	{
		$db = db_connect();
		$builder = $db->table('products as a')
			->select('a.id, a.id as action, a.name, a.price, a.description, a.thumbnail');

		$dataTable = DataTable::of($builder)
			->addNumbering('no')
			->edit('thumbnail', function($row){
				$default = base_url('no-image.png');
				$image = (!empty($row->thumbnail)) ? base_url('uploads/products/' . $row->thumbnail) : $default;

				$html = '<a href="'.$image.'" class="image-link"><img width="100" class="rounded" src="'.$default.'" id="lazy'.$row->id.'" class="lazy" data-src="'.$image.'" onerror="this.onerror=null;this.src='.$default.';" alt=""></a>';
				return $html;
			})
			->edit('action', function($row){
				$edit = '<a href="'.base_url('products/edit/'.$row->id).'" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Ubah</a>';
				$delete = '<a hrf="javascript:void()" data-href="'.base_url('products/delete/'.$row->id).'" class="btn btn-sm btn-danger remove-data"><i class="fa fa-trash"></i> Hapus</a>';
				return $edit .' '.' '. $delete;
			})
			->toJson(true);
		return $dataTable;
	}

	public function promo()
    {
		$db = db_connect();
		$model = $db->table('products');
		$query = $model;
		$query->select("*")->limit(2);

		$products = $query->orderBy('price')->get()->getResult();

        return $this->respond($products);
    }

	public function favorit()
	{
		$db = db_connect();
		$model = $db->table('products');
		$query = $model;
		$query->select("*")->limit(6);

		$products = $query->orderBy('rand()')->get()->getResult();

        return $this->respond($products);
	}
}
