<?php

namespace Products\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\Files\File;
use Products\Models\ProductModel;

class ProductsController extends BaseController
{
	protected $uploadPath;
	protected $modulePath;
	function __construct()
	{
		helper(['form', 'common', 'session']);
		$this->uploadPath = ROOTPATH . 'public/uploads/';
		$this->modulePath = ROOTPATH . 'public/uploads/products/';

		if (!file_exists($this->uploadPath)) {
			mkdir($this->uploadPath);
		}

		if (!file_exists($this->modulePath)) {
			mkdir($this->modulePath);
		}
	}
	public function index()
	{
		$model = new ProductModel();
		$data['products'] = $model->findAll();

		return view('Products\Views\index', $data);
	}

	public function create()
	{
		return view('Products\Views\create');
	}

	public function store()
	{
		$model = new ProductModel();
		$data = [
			'name' => $this->request->getVar('name'),
			'price' => $this->request->getVar('price'),
			'description' => $this->request->getVar('description'),
			'product_category_id' => $this->request->getVar('product_category_id'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		];

		// Logic Upload
		$files = $this->request->getPost('file_image');
		if ($files) {
			$thumbnail = '';
			foreach ($files as $uuid => $name) {
				if (file_exists($this->uploadPath . $name)) {
					$file = new File($this->uploadPath . $name);
					$newFileName = date('Ymd').'_'.$file->getRandomName();
					$file->move($this->modulePath, $newFileName);
					$thumbnail = $newFileName;
				}
			}
			$data['thumbnail'] = $thumbnail;
		}

		$model->insert($data);

		set_message('toastr_msg', 'Produk berhasil ditambah');
		set_message('toastr_type', 'success');

		return redirect()->to('/products');
	}

	public function edit($id)
	{
		$model = new ProductModel();
		$data['product'] = $model->find($id);

		return view('Products\Views\edit', $data);
	}

	public function update($id)
	{
		$model = new ProductModel();
		$data = [
			'name' => $this->request->getVar('name'),
			'price' => unformatRupiah($this->request->getVar('price')),
			'description' => $this->request->getVar('description'),
			'product_category_id' => $this->request->getVar('product_category_id'),
			'updated_at' => date('Y-m-d H:i:s'),
		];

		// Logic Upload
		$files = $this->request->getPost('file_image');
		if ($files) {
			$thumbnail = '';
			foreach ($files as $uuid => $name) {
				if (file_exists($this->uploadPath . $name)) {
					$file = new File($this->uploadPath . $name);
					$newFileName = date('Ymd').'_'.$file->getRandomName();
					$file->move($this->modulePath, $newFileName);
					$thumbnail = $newFileName;
				}
			}
			$data['thumbnail'] = $thumbnail;
		}

		$model->update($id, $data);

		set_message('toastr_msg', 'Produk berhasil diubah');
		set_message('toastr_type', 'success');

		return redirect()->to('/products');
	}

	public function delete($id)
	{
		$model = new ProductModel();
		$model->delete($id);

		set_message('toastr_msg', 'Produk berhasil dihapus');
		set_message('toastr_type', 'success');

		return redirect()->to('/products');
	}
}
