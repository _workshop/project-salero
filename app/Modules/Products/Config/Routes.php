<?php

namespace Products\Config;

$routes->group('products', ['namespace' => 'Products\Controllers'], function ($routes) {
    $routes->get('', 'ProductsController::index');
    $routes->get('create', 'ProductsController::create');
    $routes->post('store', 'ProductsController::store');
    $routes->get('edit/(:num)', 'ProductsController::edit/$1');
    $routes->post('update/(:num)', 'ProductsController::update/$1');
    $routes->get('delete/(:num)', 'ProductsController::delete/$1');

	$routes->add('do_init', 'ProductsController::do_init');
	$routes->add('do_upload', 'ProductsController::do_upload');
	$routes->add('do_delete', 'ProductsController::do_delete');
});

$routes->group('api/products', ['namespace' => 'Products\Controllers\Api'], function ($routes) {
	$routes->add('', 'ProductsController::index');
	$routes->add('show/(:any)', 'ProductsController::show/$1');
	$routes->add('create', 'ProductsController::create');
	$routes->add('update/(:any)', 'ProductsController::update/$1');
	$routes->add('delete/(:any)', 'ProductsController::delete/$1');
	$routes->add('datatable', 'ProductsController::datatable');
	//custom
	$routes->add('promo', 'ProductsController::promo');
	$routes->add('promo/(:any)', 'ProductsController::promo/$1');
	$routes->add('favorit', 'ProductsController::favorit');
	$routes->add('favorit/(:any)', 'ProductsController::favorit/$1');
});
