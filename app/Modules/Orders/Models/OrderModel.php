<?php

namespace Orders\Models;

use CodeIgniter\Model;

class OrderModel extends Model
{
    protected $table      = 'orders';
    protected $primaryKey = 'id';
	protected $returnType = 'object';
    protected $protectFields = false;
}
