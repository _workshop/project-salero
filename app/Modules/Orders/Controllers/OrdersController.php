<?php

namespace Orders\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\Files\File;
use Orders\Models\OrderModel;

class OrdersController extends BaseController
{
	protected $uploadPath;
	protected $modulePath;
	function __construct()
	{
		helper(['form', 'common', 'session']);
		$this->uploadPath = ROOTPATH . 'public/uploads/';
		$this->modulePath = ROOTPATH . 'public/uploads/orders/';

		if (!file_exists($this->uploadPath)) {
			mkdir($this->uploadPath);
		}

		if (!file_exists($this->modulePath)) {
			mkdir($this->modulePath);
		}
	}
	public function index()
	{
		$model = new OrderModel();
		$data['orders'] = $model->findAll();

		return view('Orders\Views\index', $data);
	}

	public function create()
	{
		return view('Orders\Views\create');
	}

	public function store()
	{
		$model = new OrderModel();
		$data = [
			'name' => $this->request->getVar('name'),
			'price' => $this->request->getVar('price'),
			'description' => $this->request->getVar('description'),
			'product_category_id' => $this->request->getVar('product_category_id'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		];

		$model->insert($data);

		set_message('toastr_msg', 'Pemesanan berhasil ditambah');
		set_message('toastr_type', 'success');

		return redirect()->to('/orders');
	}

	public function edit($id)
	{
		$model = new OrderModel();
		$data['product'] = $model->find($id);

		return view('Orders\Views\edit', $data);
	}

	public function update($id)
	{
		$model = new OrderModel();
		$data = [
			'name' => $this->request->getVar('name'),
			'price' => unformatRupiah($this->request->getVar('price')),
			'description' => $this->request->getVar('description'),
			'product_category_id' => $this->request->getVar('product_category_id'),
			'updated_at' => date('Y-m-d H:i:s'),
		];

		$model->update($id, $data);

		set_message('toastr_msg', 'Pemesanan berhasil diubah');
		set_message('toastr_type', 'success');

		return redirect()->to('/orders');
	}

	public function delete($id)
	{
		$model = new OrderModel();
		$model->delete($id);

		set_message('toastr_msg', 'Pemesanan berhasil dihapus');
		set_message('toastr_type', 'success');

		return redirect()->to('/orders');
	}
}
