<?php

namespace Orders\Controllers\Api;

use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use Orders\Models\OrderModel;
use \Hermawan\DataTables\DataTable;

class OrdersController extends BaseController
{
    use ResponseTrait;

	function __construct()
	{
		include APPPATH . 'Config/cors.php';
	}
	
    public function index()
    {
        $model = new OrderModel();
        $products = $model->findAll();

        return $this->respond($products);
    }

    public function show($id)
    {
        $model = new OrderModel();
        $product = $model->find($id);

        if ($product) {
            return $this->respond($product);
        } else {
            return $this->failNotFound('Product not found');
        }
    }

    public function create()
    {
        $model = new OrderModel();
        $data = [
            'name' => $this->request->getVar('name'),
            'price' => $this->request->getVar('price'),
            'description' => $this->request->getVar('description')
        ];

        $model->insert($data);

        $response = [
            'status' => 201,
            'error' => null,
            'message' => 'Product created successfully'
        ];

        return $this->respondCreated($response);
    }

    public function update($id)
    {
        $model = new OrderModel();
        $data = [
            'name' => $this->request->getVar('name'),
            'price' => $this->request->getVar('price'),
            'description' => $this->request->getVar('description')
        ];

        $model->update($id, $data);

        $response = [
            'status' => 200,
            'error' => null,
            'message' => 'Product updated successfully'
        ];

        return $this->respond($response);
    }

    public function delete($id)
    {
        $model = new OrderModel();
        $model->delete($id);

        $response = [
            'status' => 200,
            'error' => null,
            'message' => 'Product deleted successfully'
        ];

        return $this->respondDeleted($response);
    }

	public function datatable()
	{
		$db = db_connect();
		$builder = $db->table('orders as a')
			->select('a.id, a.id as action, a.customer_phone, a.status, "" as product_name,  a.amount');

		$dataTable = DataTable::of($builder)
			->addNumbering('no')
			->edit('action', function($row){
				$edit = '<a href="'.base_url('orders/edit/'.$row->id).'" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Ubah</a>';
				$delete = '<a hrf="javascript:void()" data-href="'.base_url('orders/delete/'.$row->id).'" class="btn btn-sm btn-danger remove-data"><i class="fa fa-trash"></i> Hapus</a>';
				return $edit .' '.' '. $delete;
			})
			->toJson(true);
		return $dataTable;
	}
}
