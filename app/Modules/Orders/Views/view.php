<?= $this->extend(config('Core')->layout_backend); ?>
<?= $this->section('style'); ?>
<?= $this->endSection('style'); ?>

<?= $this->section('page'); ?>
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-cart icon-gradient bg-strong-bliss"></i>
                </div>
                <div>Detail Order
                    <div class="page-title-subheading">Informasi Detail Pesanan</div>
                </div>
            </div>
            <div class="page-title-actions">
                <nav class="" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('auth') ?>"><i class="fa fa-home"></i> Home</a></li>
                        <li class="breadcrumb-item">Order</li>
						<li class="active breadcrumb-item" aria-current="page">Detail Order</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="card-shadow-dark profile-responsive card-border mb-3 card">
                <div class="dropdown-menu-header">
                    <div class="dropdown-menu-header-inner bg-primary">
                        <div class="menu-header-image" style="background-image: url('<?= base_url('themes/uigniter') ?>/images/dropdown-header/abstract4.jpg')"></div>
                        <div class="menu-header-content btn-pane-right">
                            <div class="avatar-icon-wrapper mr-2 avatar-icon-xl">
                                <div class="avatar-icon">

                                </div>
                            </div>
                            <div>
                                <h5 class="menu-header-title">Supplier Name</h5>
                            </div>
                            <div class="menu-header-btn-pane">
								<a data-toggle="modal" data-target="#modal_edit" href="javascript:void(0);" class="mb-2 mr-2 btn btn-pill btn-warning" title=""><i class="fa fa-maps"></i> <?=order_status_label($order->order_status)?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left mr-3">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="widget-content-left">
                                    <div class="widget-heading">Item</div>
                                </div>
                                <div class="widget-content-right">
                                    
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left mr-3">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="widget-content-left">
                                    <div class="widget-heading">Item</div>
                                </div>
                                <div class="widget-content-right">
                                    
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left mr-3">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="widget-content-left">
                                    <div class="widget-heading">Item</div>
                                </div>
                                <div class="widget-content-right">
                                    
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left mr-3">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="widget-content-left">
                                    <div class="widget-heading">Status</div>
                                </div>
                                <div class="widget-content-right">
                                    <?php if (1 == 1) : ?>
                                        <span class="mb-2 mr-2 badge badge-pill badge-success">Active</span>
                                    <?php else : ?>
                                        <span class="mb-2 mr-2 badge badge-pill badge-danger">Inactive</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left mr-3">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="widget-content-left">
                                    <div class="widget-heading">Group</div>
                                </div>
                                <div class="widget-content-right">
                                    
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>

                <div class="card-border m-3 card">
                    <div class="card-body">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left mr-3">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <div class="widget-content-left">
                                    <div class="widget-heading">Alamat</div>
                                </div>
                                <div class="widget-content-right">
                                </div>
                            </div>
                            <p class="mt-3">
										Alamt
                            </p>
                            <p class="mt-3">
                                <a href="https://maps.google.com/?q=" target="_blank" title="Lihat Google Maps" class="btn btn-sm btn-warning" style="min-width:35px"><i class="fa fa-map"> </i> Google Maps</a> 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
			<div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Riwayat Pemesanan</h5>
                    <div class="scroll-area-md">
                        <div class="scrollbar-container ps ps--active-y">
                            <div class="vertical-time-icons vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
								<div class="vertical-timeline-item vertical-timeline-element">
                                    <div>
                                        <div class="vertical-timeline-element-icon bounce-in">
                                            <div class="timeline-icon border-success bg-success">
                                                <i class="fa fa-calendar text-white"></i>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-element-content bounce-in">
                                            <h4 class="timeline-title text-secondary">Tanggal Pesan</h4>
                                            <p><?=$order->created_at?></p>

                                        </div>
                                    </div>
                                </div>
								<div class="vertical-timeline-item vertical-timeline-element">
                                    <div>
                                        <div class="vertical-timeline-element-icon bounce-in">
                                            <div class="timeline-icon border-secondary bg-secondary">
                                                <i class="fa fa-calendar text-white"></i>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-element-content bounce-in">
                                            <h4 class="timeline-title text-secondary">Bayar</h4>
                                            <p></p>

                                        </div>
                                    </div>
                                </div>
                                <div class="vertical-timeline-item vertical-timeline-element">
                                    <div>
                                        <div class="vertical-timeline-element-icon bounce-in">
                                            <div class="timeline-icon border-secondary bg-secondary">
                                                <i class="fa fa-calendar text-white"></i>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-element-content bounce-in">
                                            <h4 class="timeline-title text-secondary">Dikonfirmasi</h4>
                                            <p></p>

                                        </div>
                                    </div>
                                </div>
								<div class="vertical-timeline-item vertical-timeline-element">
                                    <div>
                                        <div class="vertical-timeline-element-icon bounce-in">
                                            <div class="timeline-icon border-secondary bg-secondary">
                                                <i class="fa fa-calendar text-white"></i>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-element-content bounce-in">
                                            <h4 class="timeline-title text-secondary">Diproses</h4>
                                            <p></p>

                                        </div>
                                    </div>
                                </div>
								<div class="vertical-timeline-item vertical-timeline-element">
                                    <div>
                                        <div class="vertical-timeline-element-icon bounce-in">
                                            <div class="timeline-icon border-secondary bg-secondary">
                                                <i class="fa fa-calendar text-white"></i>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-element-content bounce-in">
                                            <h4 class="timeline-title text-secondary">Dikirim</h4>
                                            <p></p>

                                        </div>
                                    </div>
                                </div>
								<div class="vertical-timeline-item vertical-timeline-element">
                                    <div>
                                        <div class="vertical-timeline-element-icon bounce-in">
                                            <div class="timeline-icon border-secondary bg-secondary">
                                                <i class="fa fa-calendar text-white"></i>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-element-content bounce-in">
                                            <h4 class="timeline-title text-secondary">Diterima</h4>
                                            <p></p>

                                        </div>
                                    </div>
                                </div>
                                


                            </div>
                            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                            </div>
                            <div class="ps__rail-y" style="top: 0px; right: 0px; height: 290px;">
                                <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 290px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">Info Pemesanan</h5>
                    <div class="scroll-area-md">
                        <div class="scrollbar-container ps ps--active-y">
                            <div class="vertical-time-icons vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                                <div class="vertical-timeline-item vertical-timeline-element">
                                    <div>
                                        <div class="vertical-timeline-element-icon bounce-in">
                                            <div class="timeline-icon border-secondary bg-secondary">
                                                <i class="fa fa-barcode text-white"></i>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-element-content bounce-in">
                                            <h4 class="timeline-title text-success">No Invoice</h4>
                                            <p><?=$order->invoice_no?></p>

                                        </div>
                                    </div>
                                </div>
                                <div class="vertical-timeline-item vertical-timeline-element">
                                    <div>
                                        <div class="vertical-timeline-element-icon bounce-in">
                                            <div class="timeline-icon border-secondary bg-secondary">
                                                <i class="fa fa-calendar text-white"></i>
                                            </div>
                                        </div>
                                        <div class="vertical-timeline-element-content bounce-in">
                                            <h4 class="timeline-title text-success">Tanggal Invoice</h4>
                                            <p><?=$order->created_at?></p>

                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                            </div>
                            <div class="ps__rail-y" style="top: 0px; right: 0px; height: 290px;">
                                <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 290px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection('page'); ?>

<?= $this->section('script'); ?>
<?= $this->endSection('script'); ?>