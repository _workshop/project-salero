<?php

namespace Orders\Config;

$routes->group('orders', ['namespace' => 'Orders\Controllers'], function ($routes) {
    $routes->get('', 'OrdersController::index');
    $routes->get('create', 'OrdersController::create');
    $routes->post('store', 'OrdersController::store');
    $routes->get('edit/(:num)', 'OrdersController::edit/$1');
    $routes->post('update/(:num)', 'OrdersController::update/$1');
    $routes->get('delete/(:num)', 'OrdersController::delete/$1');
});

$routes->group('api/orders', ['namespace' => 'Orders\Controllers\Api'], function ($routes) {
	$routes->add('', 'OrdersController::index');
	$routes->add('show/(:any)', 'OrdersController::show/$1');
	$routes->add('create', 'OrdersController::create');
	$routes->add('update/(:any)', 'OrdersController::update/$1');
	$routes->add('delete/(:any)', 'OrdersController::delete/$1');
	$routes->add('datatable', 'OrdersController::datatable');
});
