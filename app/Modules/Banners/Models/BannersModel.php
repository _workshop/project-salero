<?php

namespace Banners\Models;

use CodeIgniter\Model;

class BannersModel extends Model
{
    protected $table      = 'banners';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name', 'thumbnail,', 'created_at', 'updated_at'];
	protected $returnType = 'object';
    protected $protectFields = false;
}
