<?php

namespace Banners\Controllers\Api;

use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use Banners\Models\BannersModel;
use \Hermawan\DataTables\DataTable;

class BannersController extends BaseController
{
    use ResponseTrait;

	function __construct()
	{
		include APPPATH . 'Config/cors.php';
	}
	
    public function index()
    {
        $model = new BannersModel();
        $banners = $model->findAll();

        return $this->respond($banners);
    }

    public function show($id)
    {
        $model = new BannersModel();
        $product = $model->find($id);

        if ($product) {
            return $this->respond($product);
        } else {
            return $this->failNotFound('Product not found');
        }
    }

    public function create()
    {
        $model = new BannersModel();
        $data = [
            'name' => $this->request->getVar('name'),
        ];

        $model->insert($data);

        $response = [
            'status' => 201,
            'error' => null,
            'message' => 'Product created successfully'
        ];

        return $this->respondCreated($response);
    }

    public function update($id)
    {
        $model = new BannersModel();
        $data = [
            'name' => $this->request->getVar('name'),
        ];

        $model->update($id, $data);

        $response = [
            'status' => 200,
            'error' => null,
            'message' => 'Product updated successfully'
        ];

        return $this->respond($response);
    }

    public function delete($id)
    {
        $model = new BannersModel();
        $model->delete($id);

        $response = [
            'status' => 200,
            'error' => null,
            'message' => 'Product deleted successfully'
        ];

        return $this->respondDeleted($response);
    }

	public function datatable()
	{
		$db = db_connect();
		$builder = $db->table('banners as a')
			->select('a.id, a.id as action, a.name, a.thumbnail');

		$dataTable = DataTable::of($builder)
			->addNumbering('no')
			->edit('thumbnail', function($row){
				$default = base_url('no-image.png');
				$image = (!empty($row->thumbnail)) ? base_url('uploads/banners/' . $row->thumbnail) : $default;

				$html = '<a href="'.$image.'" class="image-link"><img width="100" class="rounded" src="'.$default.'" id="lazy'.$row->id.'" class="lazy" data-src="'.$image.'" onerror="this.onerror=null;this.src='.$default.';" alt=""></a>';
				return $html;
			})
			->edit('action', function($row){
				$edit = '<a href="'.base_url('banners/edit/'.$row->id).'" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Ubah</a>';
				$delete = '<a hrf="javascript:void()" data-href="'.base_url('banners/delete/'.$row->id).'" class="btn btn-sm btn-danger remove-data"><i class="fa fa-trash"></i> Hapus</a>';
				return $edit .' '.' '. $delete;
			})
			->toJson(true);
		return $dataTable;
	}
}
