<?php

namespace Banners\Config;

$routes->group('banners', ['namespace' => 'Banners\Controllers'], function ($routes) {
    $routes->get('', 'BannersController::index');
    $routes->get('create', 'BannersController::create');
    $routes->post('store', 'BannersController::store');
    $routes->get('edit/(:num)', 'BannersController::edit/$1');
    $routes->post('update/(:num)', 'BannersController::update/$1');
    $routes->get('delete/(:num)', 'BannersController::delete/$1');

	$routes->add('do_init', 'BannersController::do_init');
	$routes->add('do_upload', 'BannersController::do_upload');
	$routes->add('do_delete', 'BannersController::do_delete');
});

$routes->group('api/banners', ['namespace' => 'Banners\Controllers\Api'], function ($routes) {
	$routes->add('', 'BannersController::index');
	$routes->add('show/(:any)', 'BannersController::show/$1');
	$routes->add('create', 'BannersController::create');
	$routes->add('update/(:any)', 'BannersController::update/$1');
	$routes->add('delete/(:any)', 'BannersController::delete/$1');
	$routes->add('datatable', 'BannersController::datatable');
});
