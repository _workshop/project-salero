<?php

namespace ProductCategories\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\Files\File;
use ProductCategories\Models\ProductCategoriesModel;

class ProductCategoriesController extends BaseController
{
	protected $uploadPath;
	protected $modulePath;
	function __construct()
	{
		helper(['form', 'common', 'session']);
		$this->uploadPath = ROOTPATH . 'public/uploads/';
		$this->modulePath = ROOTPATH . 'public/uploads/productcategories/';

		if (!file_exists($this->uploadPath)) {
			mkdir($this->uploadPath);
		}

		if (!file_exists($this->modulePath)) {
			mkdir($this->modulePath);
		}
	}
	public function index()
	{
		$model = new ProductCategoriesModel();
		$data['productcategories'] = $model->findAll();

		return view('ProductCategories\Views\index', $data);
	}

	public function create()
	{
		return view('ProductCategories\Views\create');
	}

	public function store()
	{
		$model = new ProductCategoriesModel();
		$data = [
			'name' => $this->request->getVar('name'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		];

		// Logic Upload
		$files = $this->request->getPost('file_image');
		if ($files) {
			$thumbnail = '';
			foreach ($files as $uuid => $name) {
				if (file_exists($this->uploadPath . $name)) {
					$file = new File($this->uploadPath . $name);
					$newFileName = date('Ymd').'_'.$file->getRandomName();
					$file->move($this->modulePath, $newFileName);
					$thumbnail = $newFileName;
				}
			}
			$data['thumbnail'] = $thumbnail;
		}

		$model->insert($data);

		set_message('toastr_msg', 'Produk berhasil ditambah');
		set_message('toastr_type', 'success');

		return redirect()->to('/productcategories');
	}

	public function edit($id)
	{
		$model = new ProductCategoriesModel();
		$data['product'] = $model->find($id);

		return view('ProductCategories\Views\edit', $data);
	}

	public function update($id)
	{
		$model = new ProductCategoriesModel();
		$data = [
			'name' => $this->request->getVar('name'),
			'updated_at' => date('Y-m-d H:i:s'),
		];

		// Logic Upload
		$files = $this->request->getPost('file_image');
		if ($files) {
			$thumbnail = '';
			foreach ($files as $uuid => $name) {
				if (file_exists($this->uploadPath . $name)) {
					$file = new File($this->uploadPath . $name);
					$newFileName = date('Ymd').'_'.$file->getRandomName();
					$file->move($this->modulePath, $newFileName);
					$thumbnail = $newFileName;
				}
			}
			$data['thumbnail'] = $thumbnail;
		}

		$model->update($id, $data);

		set_message('toastr_msg', 'Produk berhasil diubah');
		set_message('toastr_type', 'success');

		return redirect()->to('/productcategories');
	}

	public function delete($id)
	{
		$model = new ProductCategoriesModel();
		$model->delete($id);

		set_message('toastr_msg', 'Produk berhasil dihapus');
		set_message('toastr_type', 'success');

		return redirect()->to('/productcategories');
	}
}
