<?php
$request = \Config\Services::request();
$request->uri->setSilent();
?>

<?= $this->extend('layout/sbadmin/index') ?>
<?= $this->section('style'); ?>
<!-- Dropzone CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/dropzone.min.css">
<?= $this->endSection('style'); ?>

<?= $this->section('content'); ?>
<h1 class="h3 mb-4 text-gray-800">Tambah Kategori Produk</h1>
<div class="app-page-title">
	<div class="page-title-wrapper">
		<div class="page-title-actions ml-auto">
			<nav class="" aria-label="breadcrumb">
				<ol class="breadcrumb justify-content-end">
					<li class="breadcrumb-item"><a href="<?= base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
					<li class="breadcrumb-item"><a href="<?= base_url('productcategories') ?>">Kategori Produk</a></li>
					<li class="active breadcrumb-item" aria-current="page">Tambah Kategori Produk</li>
				</ol>
			</nav>
		</div>
	</div>
</div>

<div class="main-card mb-3 card">
	<div class="card-header">
		<i class="header-icon lnr-plus-circle icon-gradient bg-plum-plate"> </i> Form Tambah Produk
	</div>
	<div class="card-body">
		<div id="infoMessage"><?= $message ?? ''; ?></div>
		<form id="frm" method="post" action="<?=base_url('productcategories/store')?>">
			<div class="form-row">
				<div class="col-md-3">
					<div class="form-group">
						<label for="name">Nama Kategori Produk*</label>
						<div>
							<input type="text" class="form-control" id="name" name="name" placeholder="Nama Produk" value="<?= set_value('name'); ?>" />
						</div>
					</div>
				</div>
			</div>

			<div class="form-row">
				<div class="col-md-12">
					<div class="position-relative form-group">
						<label for="file_image" class="">Thumbnail</label>
						<div id="file_image" class="dropzone"></div>
						<div id="file_image_listed"></div>
						<div>
							<small class="info help-block text-muted">Format (JPG|PNG). Max 2 MB</small>
						</div>
					</div>
				</div>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary" name="submit">Simpan</button>
			</div>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>
<?= $this->section('script'); ?>
<script>		
	var file_image = setDropzone('file_image', 'productcategories', '.png,.jpg,.jpeg', 1, 2);
</script>
<?= $this->endSection('script'); ?>