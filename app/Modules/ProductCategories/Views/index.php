<?= $this->extend('layout/sbadmin/index') ?>
<?= $this->section('style'); ?>
<!-- DataTables CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.19/dist/sweetalert2.min.css">

<?= $this->endSection('style'); ?>

<?= $this->section('content'); ?>
<h1 class="h3 mb-4 text-gray-800">Daftar Kategori Produk</h1>
<div class="app-page-title">
	<div class="page-title-wrapper">
		<div class="page-title-actions ml-auto">
			<nav class="" aria-label="breadcrumb">
				<ol class="breadcrumb justify-content-end">
					<li class="breadcrumb-item"><a href="<?= base_url() ?>"><i class="fa fa-home"></i> Home</a></li>
					<li class="breadcrumb-item"><a href="<?= base_url('productcategories') ?>">Produk</a></li>
					<li class="active breadcrumb-item" aria-current="page">Daftar Kategori Produk</li>
				</ol>
			</nav>
		</div>
	</div>
</div>

<div class="main-card mb-3 card">
	<div class="card-header">
		<div class="pull-right">
			<a href="<?= base_url('productcategories/create') ?>" data-toggle="tooltip" data-placement="top" title="Tambah" class=" btn btn-success" title=""><i class="fa fa-plus"></i> Tambah Kategori Produk</a>
		</div>
	</div>
	<div class="card-body table-responsive">
		<?= get_message('message'); ?>
		<table style="width: 100%;" id="tbl_product_category" class="table table-hover table-striped table-bordered">
			<thead>
				<tr>
					<th width="35" class="text-center">No</th>
					<th width="100" class="text-center">Thumbnail</th>
					<th class="text-center">Nama Kategori Produk</th>
					<th width="200" class="text-center">Aksi</th>
				</tr>
			</thead>
			<tbody id="tbl_product_category_tbody"></tbody>
		</table>
	</div>
</div>
<?= $this->endSection('content'); ?>

<?= $this->section('script'); ?>
<!-- DataTables JS -->
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
<script>
	var t;
	$(document).ready(function() {
		t = $('#tbl_product_category').DataTable({
			"serverSide": true,
			"ajax": {
				"url": '<?php echo site_url('api/productcategories/datatable'); ?>',
			},
			"pagingType": "full_numbers",
			"oLanguage": {
				"sSearch": "<i class='fa fa-search'></i> _INPUT_",
				"sLengthMenu": "_MENU_",
				"oPaginate": {
					"sNext": "<i class='fa fa-chevron-right'></i>",
					"sPrevious": "<i class='fa fa-chevron-left'></i>",
					"sLast": "<i class='fa fa-chevron-double-right'></i>",
					"sFirst": "<i class='fa fa-chevron-double-left'></i>",
				}
			},
			"columns": [{
					data: 'no',
					orderable: false
				},
				{
					data: 'thumbnail',
					orderable: false
				},
				{
					data: 'name'
				},
				{
					data: 'action',
					orderable: false,
					className: 'text-center'
				},
			],
			"order": [
				['2', 'asc']
			],
			"drawCallback": function ( data, type, full, meta ) {
				var api = this.api();
				var data = api.rows().data();
				$.each( data, function( i, row ) {
					$("#lazy"+row.id).Lazy();
				});	

				$('.image-link').magnificPopup({
					type: 'image'
				});
			},
		});
	});
</script>

<!-- Sweet Alert JS -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.19/dist/sweetalert2.min.js"></script>
<script>
	$('#tbl_product_category').on('click', '.remove-data', function() {
        var url = $(this).attr('data-href');
        Swal.fire({
            title: 'Anda yakin',
            text: "Ingin menghapus Kategori Produk ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#dd6b55',
            confirmButtonText: 'Ya, Lanjutkan',
            cancelButtonText: 'Tidak, Batalkan'
        }).then((result) => {
            if (result.value) {
                window.location.href = url;
            }
        });
        return false;
    });
</script>


<?= $this->endSection('script'); ?>