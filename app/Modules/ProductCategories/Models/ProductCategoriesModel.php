<?php

namespace ProductCategories\Models;

use CodeIgniter\Model;

class ProductCategoriesModel extends Model
{
    protected $table      = 'product_categories';
    protected $primaryKey = 'id';
    protected $allowedFields = ['name', 'thumbnail,', 'created_at', 'updated_at'];
	protected $returnType = 'object';
    protected $protectFields = false;
}
