<?php

namespace ProductCategories\Config;

$routes->group('productcategories', ['namespace' => 'ProductCategories\Controllers'], function ($routes) {
    $routes->get('', 'ProductCategoriesController::index');
    $routes->get('create', 'ProductCategoriesController::create');
    $routes->post('store', 'ProductCategoriesController::store');
    $routes->get('edit/(:num)', 'ProductCategoriesController::edit/$1');
    $routes->post('update/(:num)', 'ProductCategoriesController::update/$1');
    $routes->get('delete/(:num)', 'ProductCategoriesController::delete/$1');

	$routes->add('do_init', 'ProductCategoriesController::do_init');
	$routes->add('do_upload', 'ProductCategoriesController::do_upload');
	$routes->add('do_delete', 'ProductCategoriesController::do_delete');
});

$routes->group('api/productcategories', ['namespace' => 'ProductCategories\Controllers\Api'], function ($routes) {
	$routes->add('', 'ProductCategoriesController::index');
	$routes->add('show/(:any)', 'ProductCategoriesController::show/$1');
	$routes->add('create', 'ProductCategoriesController::create');
	$routes->add('update/(:any)', 'ProductCategoriesController::update/$1');
	$routes->add('delete/(:any)', 'ProductCategoriesController::delete/$1');
	$routes->add('datatable', 'ProductCategoriesController::datatable');
});
